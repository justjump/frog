---
name: Mary Louise
shortname: marylouise
order: 5
blurb: I’m named for Mary Louise Black, the daughter of the couple who owned the Black House. When she was growing up, a spring across the street—where Tech Bookstore is now—provided water for many people in town.
location: Alexander Black House, 204 Draper Rd.
layout: frog
---

"Alexander Black, a successful merchant and a great-grandnephew of the town’s founder, built his first house at the corner of Main and Lee Streets in 1850. (His father, Harvey, soon moved across Main Street at the intersection with Washington Street.) After his first home burned down, Alexander paid $3,500 for the construction of this Victorian mansion on the same site. The asymmetrical and ornate Queen Anne architectural style, with its turrets, stained-glass windows, and gingerbread woodwork, was rare in Appalachia. 

The Blacks moved in in 1892. Black and his wife, Elizabeth Kent Otey, adopted a daughter, Mary Louise. Born in 1890, she died in 1918 without any children. After Alexander passed away, in 1935, the property was used as a funeral parlor for more than 60 years. The town moved the house in 2002 to Draper Road, where it has been restored and preserved. 

Stroubles Creek carves a winding diagonal from Spout Spring across the downtown street grid. The stream is piped under the original site of the Black house, now the Starbucks parking lot, where the current is still audible on its course toward the Duck Pond. Next door, Town Spring provided drinking water for the early citizens of Blacksburg. As the primary spring within the original 16 squares, it was protected by a small outbuilding called a springhouse, and residents were forbidden to wash in it. Town Spring was closed in 1890 after a committee at the college called it “the source of all typhoid fever” in the area. Waste from a livery yard just up the hill—behind what is now Cabo Fish Taco—seemed to be the culprit. The stable was torn down, and the spring reopened in 1891. It dried up over the following half century, marked only by a patch of weeds by the time it was paved over for a Kroger building, which now houses Tech Bookstore. 

Take action! Cover bare spots in your lawn with mulch after reseeding, and use a straw erosion-control blanket when you till or replant your grass. "
