---
name: Nannie Bell
shortname: nanniebell
order: 9
blurb: I’m named after Nannie Bell Snell, who owned a beauty salon next door for 14 years. From where I’m sitting, Stroubles Creek runs underground until it gets to the Duckpond.
location: "Fire Station #1, 200 Progress St. NE"
layout: frog
---

Born in 1921, Nannie Bell Snell operated a beauty salon on the corner of Jackson and Progress Streets for 14 years. Snell, a graduate of the Christiansburg Institute, Bluefield State College, and Apex Beauty School in Richmond, lived across Jackson Street in one of the five historically African-American neighborhoods in Blacksburg. She played piano for St. Paul AME Church, where she was a devoted member, served as president of the Women’s Missionary Society, and volunteered with the Blacksburg booster club.

Springs in the northern part of town feed the stream that runs past the site of Snell’s salon, home to the fire station since 1958. This is the last stretch where the central branch of Stroubles flows above ground until just before it reaches the Duck Pond. Undulating brick walkways mark its path beneath the parking lot across Progress Street. From there, the water rushes under downtown Blacksburg toward an open-bottomed culvert below the southern side of the Drillfield. A three-sided pipe, eight feet high and four and a half feet wide, arches over the natural streambed until the current resurfaces from underneath a bridge on West Campus Drive. 

Just across Progress Street, the Keister-Evans spring provided water for many of Blacksburg’s early businesses. Water from the spring was piped to tanyards on College Avenue and powered the mill near the corner of College and Draper. [see Millie.]

Take action! Wash your vehicle on a flat surface that absorbs water, such as grass or gravel, so the water filters into the ground. Move your vehicle back to the pavement when you are finished.
