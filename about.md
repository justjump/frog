---

layout: about
title: About 16 Frogs
subtitle: What's this all about?
permalink: /about/
---

The 16 Frogs project is a public arts and environmental education campaign in Blacksburg, VA that seeks to restore the health of
Stroubles Creek, the broader watershed, and the water quality of downstream communities through increased
public awareness.

16 bronze frog statues are placed strategically through town and the surrounding area to call attention to the
freshwater under and around the streets and buildings of downtown Blacksburg. Each frog has been given a
name associated with an important figure in Blacksburg’s history. The frogs were created by local
artist Christine Kosiba and are modeled after native Green Frogs (Lithobates clamitans), range from 6-12”
in size, and are poised (or sometimes hiding) on culverts, stormwater drains, sidewalks and kneewalls.
The 16 Frogs committee is made up of local volunteers and Town of Blacksburg employees who are
developing educational and promotional materials for youth and adult audiences alike to make town residents
and visitors aware of the frogs and their connection to our local water resources.

A volunteer citizens’ panel guides 16 Frogs. The Town of Blacksburg oversaw the installation of the statues, and [Downtown Blacksburg, Inc.](http://www.downtownblacksburg.com/) acts as their steward.

16 Frogs is privately funded. The project was made possible by these generous contributors and collaborators:

- [Seek Education, Explore, and DiScover (SEEDS, Inc)](http://www.seedskids.org/)
- [Downtown Blacksburg, Inc.](https://www.downtownblacksburg.com/) and [Downtown Events, Inc](https://www.downtownblacksburg.com/calendar-events-blacksburg.aspx)
- [Blacksburg Museum & Cultural Foundation](https://blacksburgmuseum.org/)
- [The Lyric Theater Council](https://www.thelyric.com/)
- [Community Foundation of the New River Valley](https://cfnrv.org/)
- The [Susan Garrison](http://sustainableblacksburgva.org/about-us/susan-garrison/) Fund of the [Community Foundation of the New River Valley](https://cfnrv.org/)
- [The Town of Blacksburg](https://www.blacksburg.gov/)
- [The Biological Systems Engineering Department](https://www.bse.vt.edu/) and [Virginia Tech](https://www.vt.edu)
- [Code for New River Valley](https://codefornrv.org)

Contact [Downtown Blacksburg](http://www.downtownblacksburg.com/) to make a donation for ongoing maintenance costs and new components.

Sculptor: [Christine Kosiba](https://christinekosiba.com/)
